# GitLab Multi-Environment Preview

GitLab's
[Environment and Deployment](https://docs.gitlab.com/ce/ci/environments.html)
system supports mapping source files to deployed content:

![Previewing an asset](https://docs.gitlab.com/ce/ci/img/view_on_env_blob.png)

However, there are some limitations.
This [TamperMonkey][] script attempts to add some additional features.

<details>
<summary>Support for Multiple Environments</summary>
<p>
  GitLab's route mapping format doesn't specify which environment should be used,
  so files are always served from the last deployed environment.
</p>
<p>
  This works fine for repositories that only contain a single deployment,
  but doesn't work for repositories that deploy several times.
</p>
<p>
  For instance, you may deploy static documentation to surge.sh,
  and run one or more applications to cloud servers.
</p>
<p>
  Our route mapping allows you to specify which environment should be linked to.
</p>
</details>
<details>
<summary>Link to Multiple Assets</summary>
<p>
  GitLab uses the first route matched, and ignores all others.
</p>
<p>
  Our route mapping does this by default, but you can set
  <code>final: false</code> on specific rules so they can point to
  multiple places.
</p>
<p>
  For instance, this would allow you to link to both documentation and deployed
  assets.
</p>
</details>
<details>
<summary>Inline Preview</summary>
<p>
  Instead of just linking to external URLs,
  you can specify that external contents should be embedded as either images or
  iFrames.
</p>
</details>

## Technical Description

This [TamperMonkey][] script runs on GitLab websites.

It parses out the GitLab CI build status via site scraping and API calls
for merge requests and file previews.

GitLab CI jobs can submit
["external status" messages](https://docs.gitlab.com/ce/api/commits.html#commit-status),
which include a job name, URL, and status (pending, running, success, etc.)

The TamperMonkey script parses out this list of statuses to find one named "multi-env-preview-mapping", and uses that URL to find a route mapping.

This route mapping can then specify preview URLs based on the current file path
and the different external status states listed in GitLab CI.

## Getting Started

The [TamperMonkey][] script parses the custom route map,
determines which (if any) external URLs should be displayed,
and adds them to the GitLab interface.

- Install [TamperMonkey][]
- Visit the [script][], and click the install button

Check out [files][example-file] and [merge requests][example-mr] in our
[example repository][example-repo] to see the plugin in action!

## Repository Configuration

To use GitLab Multi-Environment Preview in your own repositories,
you'll need to setup a few files.

### Set GitLab CI Variables

You'll need a few secret GitLab CI variables:

- `GITLAB_TOKEN`
- `SURGE_LOGIN`
- `PUBLIC_SURGE_USERNAME`

### Create a Route Map

Create a route map file: `.route-map/route-map.js`.

```js
const { derivation } = require("derivable");
const routemap = require("multi-env-routemap");
const pipeline = routemap.pipeline();

const externalStage = pipeline.stages.derive("external");
const deployedDocs = externalStage.derive("docs");

routemap((route, file) => {
  file
    .is("/README.md")
    .and(deployedDocs.derive("status").is("success"))
    .derive(() => `${deployedDocs.derive("url").get()}/index.html`)
    .react(route);
});
```

To modify the route map for your own use, please read the
[route map documentation][] or see the [example route map][].

### Publish the Route Map

Add an extra stage to your `.gitlab-ci.yml` file:

```yml
publish mapping:
  stage: lint
  image: registry.gitlab.com/ryanleonard/multi-env-preview/publish-mapping
  variables:
    DOMAIN: $CI_ENVIRONMENT_SLUG-mapping.surge.sh
  environment:
    name: mapping/$CI_COMMIT_REF_NAME
    url: $CI_ENVIRONMENT_SLUG-mapping.surge.sh
```

This will use the [publish-mapping Docker image][publish-mapping] to:

- Compile `route-map.js` so it can be used by the TamperMonkey script
- Upload the compiled script to [surge.sh][]
- Store the URL to the compiled script as a build status in GitLab

### Publish Content

When you publish content or deploy an environment,
store the URL as a GitLab build status.

We provide two utilities to easily update the build status, described below.

#### Publish via Surge.sh Docker Image

If you are publishing static content on [surge.sh][], we have a Docker image
that will also update the GitLab build status:

```yml
publish content:
  stage: lint
  image: registry.gitlab.com/ryanleonard/multi-env-preview/surge
  variables:
    directory: public
    DOMAIN: $CI_ENVIRONMENT_SLUG-published.surge.sh
    STAGE_NAME: published
  environment:
    name: publish/$CI_COMMIT_REF_NAME
    url: $CI_ENVIRONMENT_SLUG-published.surge.sh
```

Set `directory` to the local directory that should be uploaded to [surge.sh][],
set `DOMAIN` to the [surge.sh][] domain to push the content to,
and use `STAGE_NAME` to name the GitLab stage.

#### Update Status via multi-env-preview-status

You can also use the [`multi-env-preview-status`][] NPM package to update the
build status on GitLab:

```yml
deploy something:
  stage: deploy
  image: node:alpine
  variables:
    DOMAIN: $CI_ENVIRONMENT_SLUG.example.com
  before_script:
    - npm install
    - $(npm bin)/multi-env-preview-status --name app running
  script:
    # Do your deployment
    - $(npm bin)/multi-env-preview-status --name app success
  environment:
    name: app/$CI_COMMIT_REF_NAME
    url: $CI_ENVIRONMENT_SLUG.example.com
```
